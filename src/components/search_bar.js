import React, { Component } from "react";

// const SearchBar = () => {
//     return <input /> //translates to React.CreateElement()
// };

class SearchBar extends Component {
    //first and only function called whenever we create new instance
    //constructor is used to initialize variables, state...
    constructor(props) {
        //calling parent method constructor()- with super();
        super(props);
        this.state = {
            //term will update as user starts typing in input
            term: ""
        }
        this.onInputChange = this.onInputChange.bind(this);
        this.onUserInputChange = this.onUserInputChange.bind(this);
    }

    render() {
        return (
            <div className="search-bar">
                <input
                    value={this.state.term}//input is now controlled component
                    onChange={this.onUserInputChange} />
            </div>
        )
    }

    onInputChange(term) {
        this.setState({ term });
        this.props.onSearchTermChange(term);

    }
    onUserInputChange(event) {
        return this.onInputChange(event.target.value);
    }
}

export default SearchBar;

