// import _ from "lodash";
import React, { Component } from "react";
import ReactDOM from "react-dom";
import SearchBar from "./components/search_bar";
import YTSearch from "youtube-api-search";
import VideoList from "./components/video_list";
import VideoDetail from "./components/video_detail";

// //first argument-object with search term and API key,
// //second arg is callback function with response;
// YTSearch({ key: API_KEY, term: "surfboard" }, function (data) {
//     console.log(data);
// })

/////////////////////////////////////////////////////////////////////
// //create a new component 
// //should produce some HTML
// const App = () => {
//     return (
//         //App renders an instance of SearchBar(written in JSX)
//         <div>
//             <SearchBar />
//         </div>
//     )
// }
//////////////////////////////////////////////////////////////////////
// //take this component's generated HTML
// //and put it, on the page (in the DOM)

// //  React.render(App);
// //Error: React is not defined

// //need to import React library...

// //still error> instantiate our Component to render in DOM...

// //instance is <App></App>

// //  ReactDOM.render(<App/>);

// //where to render component??? where on PAGE?
// //reference to root node 

// ReactDOM.render(<App />, document.querySelector(".container"));

/////////////////////////////////////////////////////////////////////




const API_KEY = "AIzaSyB_IL7HH3TWwC6gabiyQuf-1w9THUlacbo";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            videos: [],
            selectedVideo: null
        };
        this.videoSearch("hanggai");
    }



    onVideoSelect = (selectedVideo) => {
        this.setState({ selectedVideo })
    }

    startVideoSearch = (term) => {
        this.videoSearch(term);
    }


    videoSearch = (term) => {
        YTSearch({ key: API_KEY, term: term }, (videos) => {
            this.setState({
                videos: videos,
                selectedVideo: videos[0]
            });
        });
    }



    render() {
        const { videos, selectedVideo } = this.state;
        // const videoSearch = _.debounce((term) => { this.videoSearch(term) }, 300);
        const videoSearch = (term) => {
            setTimeout(() => {
                this.videoSearch(term);
            }, 300)
        };
        return (
            <div>
                <SearchBar onSearchTermChange={videoSearch} />
                <VideoDetail video={selectedVideo} />
                <VideoList
                    onVideoSelect={this.onVideoSelect}
                    videos={videos} />
            </div>
        )
    }
}
ReactDOM.render(<App />, document.querySelector(".container"));
